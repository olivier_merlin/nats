<?php

namespace Nats;

use Traversable;

/**
 * ConnectionOptions Class.
 *
 * @package Nats
 */
class ConnectionOptions
{

    /**
     * Hostname or IP to connect.
     *
     * @var string
     */
    private $host = 'localhost';

    /**
     * Port number to connect.
     *
     * @var integer
     */
    private $port = 4222;

    /**
     * Username to connect.
     *
     * @var string
     */
    private $user = null;

    /**
     * Password to connect.
     *
     * @var string
     */
    private $pass = null;

    /**
     * Token to connect.
     *
     * @var string
     */
    private $token = null;

    /**
     * Language of this client.
     *
     * @var string
     */
    private $lang = 'php';

    /**
     * Version of this client.
     *
     * @var string
     */
    private $version = '0.8.2';

    /**
     * If verbose mode is enabled.
     *
     * @var boolean
     */
    private $verbose = false;

    /**
     * If pedantic mode is enabled.
     *
     * @var boolean
     */
    private $pedantic = false;

    /**
     * If reconnect mode is enabled.
     *
     * @var boolean
     */
    private $reconnect = true;

    /**
     * Stream context to use.
     *
     * @var resource
     */
    private $streamContext = null;


    /**
     * ConnectionOptions constructor.
     *
     * <code>
     * use Nats\ConnectionOptions;
     *
     * $options = new ConnectionOptions([
     *     'host' => '127.0.0.1',
     *     'port' => 4222,
     *     'user' => 'nats',
     *     'pass' => 'nats',
     *     'lang' => 'php',
     *      // ...
     * ]);
     * </code>
     *
     * @param Traversable|array $options The connection options.
     */
    public function __construct($options = null)
    {
        //Default stream context
        $this->streamContext = stream_context_get_default();
        if (empty($options) === false) {
            $this->initialize($options);
        }
    }

    /**
     * Get the URI for a server.
     *
     * @return string
     */
    public function getAddress()
    {
        return 'tcp://' . $this->host . ':' . $this->port;
    }

    /**
     * Get the options JSON string.
     *
     * @return string
     */
    public function __toString()
    {
        $a = [
            'lang' => $this->lang,
            'version' => $this->version,
            'verbose' => $this->verbose,
            'pedantic' => $this->pedantic,
        ];
        if (empty($this->user) === false) {
            $a['user'] = $this->user;
        }
        if (empty($this->pass) === false) {
            $a['pass'] = $this->pass;
        }
        if (empty($this->token) === false) {
            $a['auth_token'] = $this->token;
        }
        return json_encode($a);
    }

    /**
     * Get host.
     *
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Set host.
     *
     * @param string $host Host.
     *
     * @return $this
     */
    public function setHost(string $host): ConnectionOptions
    {
        $this->host = $host;

        return $this;
    }


    /**
     * Get port.
     *
     * @return integer
     */
    public function getPort(): int
    {
        return $this->port;
    }


    /**
     * Set port.
     *
     * @param integer $port Port.
     *
     * @return $this
     */
    public function setPort(int $port): ConnectionOptions
    {
        $this->port = $port;

        return $this;
    }


    /**
     * Get user.
     *
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }


    /**
     * Set user.
     *
     * @param string $user User.
     *
     * @return $this
     */
    public function setUser(string $user): ConnectionOptions
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Get password.
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set password.
     *
     * @param string $pass Password.
     *
     * @return $this
     */
    public function setPass(string $pass): ConnectionOptions
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Set token.
     *
     * @param string $token Token.
     *
     * @return $this
     */
    public function setToken(string $token): ConnectionOptions
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }


    /**
     * Set language.
     *
     * @param string $lang Language.
     * @return $this
     */
    public function setLang(string $lang): ConnectionOptions
    {
        $this->lang = $lang;

        return $this;
    }


    /**
     * Get version.
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }


    /**
     * Set version.
     *
     * @param string $version Version number.
     *
     * @return $this
     */
    public function setVersion(string $version): ConnectionOptions
    {
        $this->version = $version;

        return $this;
    }


    /**
     * Get verbose.
     *
     * @return boolean
     */
    public function isVerbose(): bool
    {
        return $this->verbose;
    }


    /**
     * Set verbose.
     *
     * @param boolean $verbose Verbose flag.
     *
     * @return $this
     */
    public function setVerbose(bool $verbose): ConnectionOptions
    {
        $this->verbose = $verbose;

        return $this;
    }


    /**
     * Get pedantic.
     *
     * @return boolean
     */
    public function isPedantic(): bool
    {
        return $this->pedantic;
    }


    /**
     * Set pedantic.
     *
     * @param boolean $pedantic Pedantic flag.
     *
     * @return $this
     */
    public function setPedantic(bool $pedantic): ConnectionOptions
    {
        $this->pedantic = $pedantic;

        return $this;
    }


    /**
     * Get reconnect.
     *
     * @return boolean
     */
    public function isReconnect(): bool
    {
        return $this->reconnect;
    }


    /**
     * Set reconnect.
     *
     * @param boolean $reconnect Reconnect flag.
     *
     * @return $this
     */
    public function setReconnect(bool $reconnect): ConnectionOptions
    {
        $this->reconnect = $reconnect;

        return $this;
    }

    /**
     * Get stream context.
     *
     * @return resource
     */
    public function getStreamContext()
    {
        return $this->streamContext;
    }

    /**
     * Set stream context.
     *
     * @param resource $streamContext Stream context.
     *
     * @return $this
     */
    public function setStreamContext($streamContext): ConnectionOptions
    {
        $this->streamContext = $streamContext;

        return $this;
    }

    /**
     * Set the connection options.
     *
     * @param Traversable|array $options The connection options.
     *
     * @return void
     */
    public function setConnectionOptions(array $options): void
    {
        $this->initialize($options);
    }

    /**
     * Initialize the parameters.
     *
     * @param Traversable|array $options The connection options.
     * @return void
     * @throws Exception When $options are an invalid type.
     * @author ikubicki
     */
    protected function initialize($options): void
    {
        if (is_iterable($options)) {
            throw new Exception('The $options argument must be iterable!');
        }

        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $method = 'set' . $key;
                $this->$method($value);
            }
        }
    }
}
